import time
import pyatspi
from pyatspi.Accessibility import Accessible
from src import collision
from src.config import configuration

from typing import List
import libSharedMat as shmMat
import numpy as np

import cv2
import speechd

configuration.speech = speechd.SSIPClient('tactos')
configuration.speech.set_output_module('festival')
configuration.speech.set_language('fr')
configuration.speech.set_priority(speechd.Priority.IMPORTANT)
configuration.last_speech = "" # last element said to avoid repetition

configuration.load_config()
# get the Registry singleton 
reg = pyatspi.Registry() 

# and desktop 
desktop = reg.getDesktop(0)
# for debug purpose
max_val = 0
n = 1
moy = 0.05
##################
shared_mat = shmMat.SharedMatWriter("tactos", configuration.sharedMat['size'])

# events 
def mousecallback(event):
  #debug inforamtion
  t1 = time.perf_counter()
  #debug inforamtion
  is_in_focus(event.detail1, event.detail2)
  #debug inforamtion
  t2 = time.perf_counter()
  t = t2-t1
  global max_val
  global n
  global moy
  max_val = max([max_val, t])
  moy = (moy*n+t)/(1+n)
  n  = n+1
  #debug inforamtion
  #print("time duration : {}".format(t))
  
def eventCallback(event): 
  """
  @brief update the active window
  :param event: event captured
  """
  if event.type == "window:activate":
    configuration.active_window = event.source
    #global focus_window
    #focus_window = event.source

def on_key_input(event):
  step = configuration.shortcuts['matrix-size']
  if event.event_string=='Page_Up': # alt+Page_Up
    if configuration.window["width"] >= step:
      configuration.window["width"] += step 
      configuration.window["height"] += step 
    print ("updating matrix size to [{}, {}]".format(configuration.window["width"],
                                                     configuration.window["height"]))
  if event.event_string=='Page_Down': # alt+Page_Down
    configuration.window["width"] -= step 
    configuration.window["height"] -= step 
    print ("updating matrix size to [{}, {}]".format(configuration.window["width"],
                                                     configuration.window["height"]))


def get_children_list(access: Accessible, x: int, y: int, coord_type: bool)-> List[Accessible]:
  """
  @brief recursive function to get list of children
  containing the point (x,y)
  :param access: curretn Accessible 
  :param x: x coordinate
  :param y: y coordinate
  :param coord_type: p_coord_type:...
  :type coord_type: t_coord_type:int
  :returns: r:...
  """
  result = access.get_accessible_at_point(x,y,coord_type)
  if not result or result == access:
    print(access.getRole())
    if access.getRole() in configuration.roles:
      if configuration.last_speech != access.name:
        configuration.speech.cancel()
        configuration.speech.speak(access.name)
        configuration.last_speech = access.name
      print("el name : " + access.name)
      return [access]
    else:
      return []
  else:
    children = get_children_list(result, x, y, coord_type)
    if access.getRole() in configuration.roles:
      return [access]+ children
    else:
      return children

def is_in_focus(x: int ,y: int):
  """
  @brief update shared matrix.
  :param x: x coordinate
  :param y: y coordinate
  """
  win = configuration.active_window
  
  if win.contains(x,y,0):
    box_list = get_children_list(win, x, y, 0)
    w = configuration.window["width"]
    h = configuration.window["height"]
    cursor_area = collision.Rectangle.fromCenter(x,y, w = w, h = h)
    matrix = np.full((w, h), True, dtype=bool)
    # check for intersection between detection window and Accessible borders
    for box in box_list:
      rect_box = collision.Rectangle.fromAccessible(box)
      col, result = cursor_area.collision_border(rect_box)
      if not col:
        break # FEATURE add other children detection to se if they are in the area
      matrix = np.logical_and(matrix, result == 1, dtype=bool)

    _, resized_matrix = cv2.threshold(matrix.astype(np.uint8),
                                      0,255,cv2.THRESH_BINARY)
    out = cv2.resize(resized_matrix, (int(configuration.sharedMat['size']/4), 4), interpolation = cv2.INTER_AREA)
    #debug informations
    _, out2 = cv2.threshold(out,254,255,cv2.THRESH_BINARY)
    cv2.imshow("all", resized_matrix)
    out3 = cv2.resize(out2, (100, 100), interpolation = cv2.INTER_AREA)
    cv2.imshow("threshold", out3)
    #debug informations
    global shared_mat
    shared_mat.write(out.flatten() < 254)
    
  else:
    shared_mat.write(np.zeros((configuration.sharedMat['size'],1)))
    print("out of the box")


what = ['window:activate'] 
print ('Monitoring window events' )
reg.registerEventListener(eventCallback, *what) 
reg.registerEventListener(mousecallback, 'mouse:abs')

mask = 1 << pyatspi.MODIFIER_META # magic number to get key event
mask+= 1 << pyatspi.MODIFIER_ALT
pyatspi.Registry.registerKeystrokeListener(on_key_input, kind=(pyatspi.KEY_PRESSED_EVENT,), mask=mask)
try: 
  configuration.speech.speak("tactos operationnel")
  pyatspi.Registry.start()
except KeyboardInterrupt:
  pass 
 
#cleanup 
pyatspi.Registry.stop() 
reg.deregisterEventListener(eventCallback, *what) 
configuration.speech.close()
print(max_val)
print(moy)
print ('Done')
