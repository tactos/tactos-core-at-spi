import pytest
import numpy as np
from src import collision

def test_rectangles_without_collision():
  rect = collision.Rectangle.fromCenter(50,50,50,50)
  cursor = collision.Rectangle.fromCenter(150,150,50,50)
  col,mat = cursor.collision_border(rect)
  assert col is False
  assert np.array_equal(np.ones((50,50)), mat)


def test_rectangles_with_left_top_corner_collision():
  rect = collision.Rectangle.fromCenter(95,95,20,10)
  cursor = collision.Rectangle.fromCenter(100,100,20,10)
  col,mat = cursor.collision_border(rect)
  assert col is True
  result = np.ones((10,20))
  result[5, :16] = 0
  result[:6, 15] = 0
  print(result)
  assert np.array_equal(result, mat)
  
def test_rectangles_with_right_bottom_corner_collision():
  rect = collision.Rectangle.fromCenter(95,95,20,10)
  cursor = collision.Rectangle.fromCenter(100,100,20,10)
  col,mat = rect.collision_border(cursor)
  assert col is True
  result = np.ones((10,20))
  result[5, 5:] = 0
  result[5:, 5] = 0
  print(result)
  assert np.array_equal(result, mat)
  
def test_rectangles_with_win_inside_cursor():
  rect = collision.Rectangle.fromCenter(100,100,10,10)
  cursor = collision.Rectangle.fromCenter(100,100,20,16)
  col,mat = cursor.collision_border(rect)
  assert col is True
  result = np.ones((16,20))
  result[3, 5:16] = 0
  result[13, 5:16] = 0
  result[3:14, 5] = 0
  result[3:14, 15] = 0
  print(result)
  assert np.array_equal(result, mat)
  
def test_rectangles_with_win_half_inside_cursor():
  rect = collision.Rectangle.fromCenter(100,105,10,10)
  cursor = collision.Rectangle.fromCenter(100,100,20,10)
  col,mat = cursor.collision_border(rect)
  assert col is True
  result = np.ones((10,20))
  result[5, 5:16] = 0
  result[5:, 5] = 0
  result[5:, 15] = 0
  print(result)
  assert np.array_equal(result, mat)
  
def test_rectangles_with_cursor_inside_win():
  rect = collision.Rectangle.fromCenter(100,100,20,16)
  cursor = collision.Rectangle.fromCenter(100,100,10,10)
  col,mat = cursor.collision_border(rect)
  assert col is True
  assert np.array_equal(np.ones((10,10)), mat)
  
def test_rectangles_with_win_half_inside_cursor():
  cursor = collision.Rectangle.fromCenter(100,105,10,10)
  rect = collision.Rectangle.fromCenter(100,100,20,10)
  col,mat = cursor.collision_border(rect)
  assert col is True
  result = np.ones((10,10))
  result[5, :] = 0
  print(result)
  assert np.array_equal(result, mat)
