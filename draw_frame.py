
import argparse
import pyatspi
import Xlib.display
import PIL.Image
from src.config import configuration

parser = argparse.ArgumentParser(description="Draw image from window. Rightclick to select window")     
parser.add_argument("--config", help="Path to a config file")
parser.add_argument("--output", help="choose output base name. name will be [output]X.png")
parser.add_argument("--color", help="enable experimental color support", action="store_true")

args = parser.parse_args()

if args.output:
  configuration.output = args.output
else:
  configuration.output = 'img'
  
if args.config:
  configuration.load_config(args.config)

disp = Xlib.display.Display()
screen = disp.screen()

screen_width = screen.width_in_pixels
screen_height = screen.height_in_pixels
frame = None
i = 0

def fix(a, limit):
    if a < 0:
        return 0
    if a >= limit:
        return limit-1
    return a

def fixX(x):
    return fix(x, screen_width)

def fixY(y):
    return fix(y, screen_height)

def define_color(value : int):
  """
  @brief create color according to initial value between 0 and 125.
  @details this function create a deterministe color from value with 
  regular interval and far enough from white
  :param value: value between 0 and 125
  :returns: RGB tuple
  """
  r = g = b = 0
  i = value*5
  print(i)
  j = i - 210
  print(b)
  if j > 0:
    print("b>0")
    r = 210
    k = j - 210
    if k > 0:
      g = 210
      b = k
    else:
      g = j
  else:
    print("not b>0")
    r = i
  return (r,g,b)

def rectangle(i, x1, x2, y1, y2, color):
    x1 = fixX(x1)
    x2 = fixX(x2)
    y1 = fixY(y1)
    y2 = fixY(y2)
    for x in range(x1, x2+1):
        i.putpixel((x, y1), color)
        i.putpixel((x, y2), color)

    for y in range(y1+1, y2):
        i.putpixel((x1, y), color)
        i.putpixel((x2, y), color)
      
def draw_access(access, output):
    component = access.queryComponent()
    (x, y, width, height) = component.getExtents(0)
    color = (0,0,0)
    try:
      if args.color:
        color = define_color(access.getRole())
    except:
        pass
    print("draw %dx%d+%d+%d with %s color\n" % (width, height, x, y, color))
    rectangle(output, x, x+width-1, y, y+height-1, color)

def draw_elements(access, output):
    if not access.getState().contains(pyatspi.STATE_SHOWING):
        return
    if access.getRole() in configuration.roles:
        draw_access(access, output)
    for child in access:
        draw_elements(child, output)

def eventCallback(event):
  #print(event)
  global frame
  if event.type == "mouse:button:3p":
    global i
    output =  PIL.Image.new("RGB", (screen_width, screen_height), (255, 255, 255))
    draw_elements(frame, output)
    pos = frame.get_position(0)
    size = frame.get_size()
    out = output.crop((pos.x,pos.y, pos.x+size.x, pos.y+size.y))
    out.save(configuration.output+str(i)+".png")
    i += 1
    out.show()
    print(event.source)
    
def windowCallback(event):
  print("focus : ", event.source)
  global frame
  frame = event.source

reg = pyatspi.Registry()
reg.registerEventListener(eventCallback, "mouse:button")
reg.registerEventListener(windowCallback, "window:activate")
try: 
  print("right click to focus window to save it to " + configuration.output+"X.png")
  pyatspi.Registry.start()
except KeyboardInterrupt:
  pass 

pyatspi.Registry.stop() 
reg.deregisterEventListener(eventCallback, "mouse:button") 
