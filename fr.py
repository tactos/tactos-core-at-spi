#!/usr/bin/python3

import pyatspi
import Xlib
import Xlib.display
import PIL.Image
import sys
import time
import cv2
import numpy as np
import subprocess


disp = Xlib.display.Display()
screen = disp.screen()

screen_width = screen.width_in_pixels
screen_height = screen.height_in_pixels

output = np.zeros((screen_height, screen_width))
p = None

def fix(a, limit):
    if a < 0:
        return 0
    if a >= limit:
        return limit-1
    return a

def fixX(x):
    return fix(x, screen_width)

def fixY(y):
    return fix(y, screen_height)


def rectangle(i, x1, x2, y1, y2):
    x1 = fixX(x1)
    x2 = fixX(x2)
    y1 = fixY(y1)
    y2 = fixY(y2)
    i[y1, x1: x2+1] = 0
    i[y2, x1: x2+1] = 0
      
    i[y1+1: y2, x1] = 0
    i[y1+1: y2, x2] = 0
      
def draw_access(access):
    component = access.queryComponent()
    (x, y, width, height) = component.getExtents(0)
    print("draw %dx%d+%d+%d\n" % (width, height, x, y))
    rectangle(output, x, x+width-1, y, y+height-1)

def draw_elements(access):
    if not access.getState().contains(pyatspi.STATE_SHOWING):
        return
    print("found element", access.getRole())
    #if access.getRole() in \
            #[ pyatspi.ROLE_CHECK_BOX, 
              #pyatspi.ROLE_PUSH_BUTTON, 
              #pyatspi.ROLE_TOGGLE_BUTTON,

              #pyatspi.ROLE_TREE_TABLE,
              #pyatspi.ROLE_TABLE,

              #pyatspi.ROLE_LABEL, 
              #pyatspi.ROLE_ENTRY,
              #pyatspi.ROLE_TEXT,

              #pyatspi.ROLE_FRAME ]:
        #print("draw it")
    draw_access(access)
    for child in access:
        draw_elements(child)

def eventCallback(event): 
  #output.close()
  print(event.source.name)
  if event.source.name == "Terminal" :
    print("Terminal")
    return 
  global output,p
  output = np.ones((screen_height, screen_width))
  draw_elements(event.source)
  
  out = cv2.resize(output, (1920, 540), interpolation = cv2.INTER_AREA)  
  cv2.imshow('image',out)


what = ['window:activate']
reg = pyatspi.Registry()
desktop = reg.getDesktop(0)
reg.registerEventListener(eventCallback, *what)

try: 
  pyatspi.Registry.start()
except KeyboardInterrupt:
  pass 
 
#cleanup 
pyatspi.Registry.stop() 
reg.deregisterEventListener(eventCallback, *what) 
print ('Done')
