import numpy as np
from .config import configuration

from typing import Tuple, Optional
from pyatspi.Accessibility import Accessible

class Rectangle(object):
  x = 0
  y = 0
  width = 0
  height = 0
 
  @classmethod
  def fromCenter(cls, x: int, y: int, w: Optional[int] = None, h: Optional[int] = None):
    width = w or configuration.window["width"]
    height = h or configuration.window["height"]
    return cls(int(x-width/2), int(y-height/2), width, height)
  
  @classmethod
  def fromAccessible(cls, accessible: Accessible):
    pos = accessible.get_position(0)
    size = accessible.get_size()
    return cls(pos.x, pos.y, size.x, size.y)
  
  def __init__(self, x = 0, y = 0,
               width: int = None ,
               height: int = None):
    self.x = x
    self.y = y
    self.width = width or configuration.window["height"]
    self.height = height or configuration.window["width"]

  def __str__(self):
    return 'origin : [{},{}], size : [{},{}]'.format(self.x, self.y,
                                                     self.width, self.height)


  def collision_border(self, rect_win) -> Tuple[bool, np.array]:
    """
    @brief check if rect_win is collisionning with self. 
    :param rect_win: rectangle that can be in collision with self
    :returns: (collision, matrix) collision is true if there was collision, matrix is a 
    bool matrix with collision pixels
    """
    matrix = np.ones((self.height, self.width))
    collision = False
    X_max = min(rect_win.x+rect_win.width, self.x+self.width) - self.x
    X_min = max(rect_win.x, self.x) - self.x
    Y_min = max(rect_win.y, self.y) - self.y
    Y_max = min(rect_win.y+rect_win.height, self.y + self.height) - self.y
    if (X_min <= X_max and Y_min <= Y_max): # collision in X and Y axis
      
      collision = True
      if Y_min != 0: # Y_min = rect_win.minY
        matrix[Y_min,X_min:X_max] = 0
      if Y_max != self.height: # Y_min = rect_win.minY
        matrix[Y_max,X_min:X_max] = 0
      if X_min != 0: # X_min = rect_win.minX
        matrix[Y_min:Y_max+1, X_min] = 0
      if X_max != self.width:
        matrix[Y_min:Y_max+1, X_max] = 0
    return (collision, matrix)
