import os 
from typing import Dict, Optional
import pyatspi
from ruamel.yaml import YAML

CONFIG_PATH = "config/default.yml"

def get_key(dct: Dict, value):
  for key, val in dct.items():
    if val == value:
      return key
  raise  ValueError("value {} not found".format(value))

class Config(object):
  
  def __init__(self):
    """
    @brief create default configuration : 
    
    .. code-block:: yaml
        
        Window:
          width: 32
          heigth: 32
          
        Matrix:
          name: tactos
          size: 16
          
        Roles:
          - frame
    """
    self.roles = ["frame"]
    self.sharedMat = {"name": "tactos", "size": 16}
    self.window = {"width" : 32, "heigth" : 32}
    self.shortcuts = {"matrix-size": 12}
    self.get_active_window()

  def load_config(self, url: Optional[str] = None):
    """     parse configuration file and save informations in a structure like :
    .. code-block:: yaml"""     
    url = url or CONFIG_PATH
    yaml = YAML(typ='safe')
    if os.path.isfile(url):
      # parse config.yml arguments
      with open(url) as file:
        yaml_data = yaml.load(file)
        
        # capture window size
        self.window = yaml_data['Window']
        self.sharedMat = yaml_data['Matrix']
        self.shortcuts = yaml_data['Shortcuts']
        lst_roles = list()
        for el in yaml_data['Roles']:
          lst_roles.append(get_key(pyatspi.ROLE_NAMES, el))
        self.roles = lst_roles

  def get_active_window(self):
    """
    @brief get the window that currently own the focus and save it
    in self.active_window
    """
    desk = pyatspi.Registry().getDesktop(0)
    for app in desk:
      for win in app:
        if win.states and win.states.contains(pyatspi.STATE_ACTIVE):
          self.active_window = win
          return 
        if app.states and app.states.contains(pyatspi.STATE_ACTIVE):
          self.active_window = app
          return
  
  def __str__(self):
      roles_str = list()
      for role in self.roles:
        roles_str.append(pyatspi.Role(role).__str__())
      
      return 'window : {}\nsharedMat : {}\nroles : \n\t{}'.format(self.window,
                                                                self.sharedMat,
                                                                '\n\t'.join(roles_str))
      
configuration = Config()
